// Поясніть своїми словами, як ви розумієте, що таке деструктуризація і навіщо вона потрібна.
// Деструктуризація - це можливість просвоїти змінним вміст об'єктів, масивів.
// Інколи це дозволяє зручніше виконувати завдання, в яких використовуються об'єкти і масиви.



// -----Завдання 1-----
// Дві компанії вирішили об'єднатись, і для цього їм потрібно об'єднати базу даних своїх клієнтів.
// У вас є 2 масиви рядків, у кожному з них – прізвища клієнтів. Створіть на їх основі один масив,
// який буде об'єднання двох масивів без повторюваних прізвищ клієнтів.

const clients1 = ["Гилберт", "Сальваторе", "Пирс", "Соммерс", "Форбс", "Донован", "Беннет"];
const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];

//Перший варіант вирішення
function unitedCo1(clients1, clients2) {
    let arr = [];
    for (let elem1 of clients1) {
        let match = 0;
        for (let elem2 of clients2) { 
            if (elem2 === elem1) { 
                match = 1;
            }
        }
        if (match === 0) { 
            arr.push(elem1);
        }
    }
    return [...arr, ...clients2];
}
// console.log(unitedCo1(clients1, clients2));

//Другий варіант вирішення
function unitedCo2(clients1, clients2) {
    let arr = [... clients1, ... clients2];
    return [... new Set(arr)];
}

let clients = unitedCo2(clients1, clients2);
// console.log(clients);

// -----Завдання 2-----
// Перед вами массив characters, що складається з об'єктів. Кожен об'єкт описує одного персонажа.
// Створіть на його основі масив charactersShortInfo, що складається з об'єктів, у яких є тільки 3 поля - ім'я, прізвище та вік.

const characters = [
  {
    name: "Елена",
    lastName: "Гилберт",
    age: 17, 
    gender: "woman",
    status: "human"
  },
  {
    name: "Кэролайн",
    lastName: "Форбс",
    age: 17,
    gender: "woman",
    status: "human"
  },
  {
    name: "Аларик",
    lastName: "Зальцман",
    age: 31,
    gender: "man",
    status: "human"
  },
  {
    name: "Дэймон",
    lastName: "Сальваторе",
    age: 156,
    gender: "man",
    status: "vampire"
  },
  {
    name: "Ребекка",
    lastName: "Майклсон",
    age: 1089,
    gender: "woman",
    status: "vempire"
  },
  {
    name: "Клаус",
    lastName: "Майклсон",
    age: 1093,
    gender: "man",
    status: "vampire"
  }
];

function shortInfo(characters) {
    let arr = [];
    for (let obj of characters) { 
        let { name, lastName, age } = obj;    
        arr.push({ name, lastName, age });
    }   
    return arr;
}

let charactersShortInfo = shortInfo(characters);
// console.log(charactersShortInfo);


// -----Завдання 3-----
// У нас є об'єкт' user:

const user1 = {
  name: "John",
  years: 30,
};

// Напишіть деструктуруюче присвоєння, яке:
// властивість name присвоїть в змінну ім'я
// властивість years присвоїть в змінну вік
// властивість isAdmin присвоює в змінну isAdmin false, якщо такої властивості немає в об'єкті
// Виведіть змінні на екран.

let { name, years, isAdmin = false } = user1;
document.querySelector("body").innerHTML += `
    <ul>
        <li>${name}</li>
        <li>${years}</li>
        <li>${isAdmin}</li>
    </ul>
`;


// -----Завдання 4-----
// Детективне агентство кілька років збирає інформацію про можливу особистість Сатоши Накамото.Вся інформація,
// зібрана у конкретному році, зберігається в окремому об'єкті. Усього таких об'єктів три - satoshi2018, satoshi2019, satoshi2020.
// Щоб скласти повну картину та профіль, вам необхідно об'єднати дані з цих трьох об'єктів в один об'єкт - fullProfile.
// Зверніть увагу, що деякі поля в об'єктах можуть повторюватися. У такому випадку в результуючому об'єкті має зберегтися значення,
// яке було отримано пізніше(наприклад, значення з 2020 пріоритетніше порівняно з 2019).
// Напишіть код, який складе повне досьє про можливу особу Сатоші Накамото. Змінювати об'єкти satoshi2018, satoshi2019, satoshi2020 не можна.
const satoshi2020 = {
  name: 'Nick',
  surname: 'Sabo',
  age: 51,
  country: 'Japan',
  birth: '1979-08-21',
  location: {
    lat: 38.869422, 
    lng: 139.876632
  }
}

const satoshi2019 = {
  name: 'Dorian',
  surname: 'Nakamoto',
  age: 44,
  hidden: true,
  country: 'USA',
  wallet: '1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa',
  browser: 'Chrome'
}

const satoshi2018 = {
  name: 'Satoshi',
  surname: 'Nakamoto', 
  technology: 'Bitcoin',
  country: 'Japan',
  browser: 'Tor',
  birth: '1975-04-05'
}

function objAdd(obj_from = {}, obj_to = {}) {

    let obj_res = {};

    for (let [key, val] of Object.entries(obj_from)) { 
        obj_res[`${key}`] = val;
    }
    for (let [key, val] of Object.entries(obj_to)) { 
        obj_res[`${key}`] = val;
    }
    return obj_res;
}

let fullProfile = objAdd(objAdd(satoshi2018, satoshi2019), satoshi2020);

// console.log(fullProfile);

// -----Завдання 5-----
// Дано масив книг. Вам потрібно додати до нього ще одну книгу, не змінюючи існуючий масив (в результаті операції має бути створено новий масив).
const books = [{
    name: 'Harry Potter',
    author: 'J.K. Rowling'
  }, {
    name: 'Lord of the rings',
    author: 'J.R.R. Tolkien'
  }, {
    name: 'The witcher',
    author: 'Andrzej Sapkowski'
  }];
  
  const bookToAdd = {
    name: 'Game of thrones',
    author: 'George R. R. Martin'
  }

// Перший вараіант вирішення
  let newBooks = [...books, ...[bookToAdd]];
//   console.log(newBooks);

// Другий вараіант вирішення
  let [one, two, three] = books;
  let four = bookToAdd;
  let newBooks2 = [one, two, three, four];
//   console.log(newBooks2);


// -----Завдання 6-----
// Даний об'єкт employee. Додайте до нього властивості age і salary, не змінюючи початковий об'єкт (має бути створено 
// новий об'єкт, який включатиме всі необхідні властивості). Виведіть новий об'єкт у консоль.

const employee = {
  name: 'Vitalii',
  surname: 'Klichko'
}

let {name:name2, surname} = employee;
let age = 44;
let salary = 10000;
let newEmployee = {name2, surname, age, salary};

console.log(newEmployee);


// -----Завдання 7-----
// Доповніть код так, щоб він коректно працював

const array = ['value', () => 'showValue'];

// Допишіть код тут
let [value, showValue] = array;

alert(value); // має бути виведено 'value'
alert(showValue());  // має бути виведено 'showValue'